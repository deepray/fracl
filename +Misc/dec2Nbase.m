function baseN = dec2Nbase(x,start,base,len)

baseN=ones(1,len);

x    = x-start;
ind  = len;
while(x>0)
    r = mod(x,base);
    baseN(1,ind) = r + start;
    x = (x-r)/base;
    ind = ind-1;
end


return