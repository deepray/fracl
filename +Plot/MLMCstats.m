% Plots solution at final time

function MLMCstats(StatL,config)

Tf = config.Tf;

figure(1)
clf
x     = StatL{3};
mean  = StatL{1};
std   = sqrt(StatL{2});

hold all
hf = fill([x',fliplr(x')],[(mean+std)',fliplr((mean-std)')],[0.8 0.8 0.9]);
h1 = plot(x,mean,'k','LineWidth',2);
h2 = plot(x,mean+std,'r-.','LineWidth',2);
h3 = plot(x,mean-std,'b--','LineWidth',2);
legend([h1,h2,h3],{'Mean','Mean + std','Mean - std'},'Location','northeast')
set(hf,'EdgeColor','None');
hold off


set(gca,'fontsize',20)
xlabel('x')
%title(['Statistics at time t = ',num2str(Tf)])
box
outfile = sprintf('Output/MLMC_Variance.pdf');
axis tight
%ylim([-1.3,1.3])
print('-dpdf', outfile)

end