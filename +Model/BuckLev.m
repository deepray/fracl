classdef BuckLev < Model.ModelBase
    %Buckley-Leverett equation
    
    properties (SetAccess = private)
        name = 'BuckLev'
    end
    
%     methods
%         function ret = f(obj, U, omega)
%             if(~isempty(omega))
%                 p = 1.5 + omega(1);
%             else
%                 p = 2.1;
%             end
%             ret = U.^p./(U.^p + (1-U).^p);
%             %fprintf('f %d %d %f\n',isreal(U),isreal(ret), min(U))
%         end
%     end

%     methods
%         function ret = df(obj, U, omega)
%             if(~isempty(omega))
%                 p = 1.5 + omega(1);
%             else
%                 p = 2.1;
%             end
%             ret = p*U.^(p-1).*(1-U).^(p-1)./(U.^p + (1-U).^p).^2;
%             %fprintf('df %d %d %f\n',isreal(U),isreal(ret), min(U))
%         end
%     end
    
    methods
        function ret = f(obj, U, omega)
            if(~isempty(omega))
                a = 0.3 + 0.4*omega(1);
            else
                a = 0.5;
            end
            ret = U.^2./(U.^2 + a*(1-U).^2);
            %fprintf('f %d %d %f\n',isreal(U),isreal(ret), min(U))
        end
    end
    
    methods
        function ret = df(obj, U, omega)
            if(~isempty(omega))
                a = 0.3 + 0.4*omega(1);
            else
                a = 0.5;
            end
            ret = 2*a*U.*(1-U)./(U.^2 + a*(1-U).^2).^2;
            %fprintf('df %d %d %f\n',isreal(U),isreal(ret), min(U))
        end
    end
    
    methods
        function ret = nflux(obj, U, omega) 
            alam = abs(obj.df(U,omega));
            ret = 0.5*( obj.f(U(2:end),omega) + obj.f(U(1:end-1),omega) ...
                - (U(2:end)- U(1:end-1)).*max(alam(1:end-1),alam(2:end)) );
            %pause(0.01)
        end
    end
    
end