function burgersMLMC_stat_rmse()
	%% Set configurations
	config = Config;

	config.model = Model.Burgers;
	config.Tf    = 0.5;
	config.CFL   = 0.8;
	config.L     = 1;	
    config.Nc0   = 51;
    config.Levels= 3;
    
	config.n_ic_rvar = 0;
    config.ic    = @(x,omega) 1*(x<0.1) - 1*(x>=0.1);
 
    config.bc    = 'Transmissive';
    config.n_A_rvar = 1;
    config.A     = @(U,omega) max(U,omega);
    config.dA    = @(U,omega) 0*(U<omega) + 1*(U>=omega);
    config.Alpha = 0.75; 
    config.advection = true;
    config.scheme = @Scheme.implicit_scheme;
    config.exp_ref_file = 'Reference/ref1.mat';

	%% Run solver
	[Errors] = Solver.MLMC_stat_rmse(config,10)
    
    figure(1)
    plot(log(Errors(:,1)),log(Errors(:,3)))
    polyfit(log(Errors(:,1)),log(Errors(:,3)),1)
    figure(2)
    plot(log(Errors(:,2)),log(Errors(:,3)))
    polyfit(log(Errors(:,2)),log(Errors(:,3)),1)
    
	%% Display data
	%Plot.Stat_errors(Errors);


end