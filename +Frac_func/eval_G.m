function [G,Gb] = eval_G(L,N,Alpha)

% Evaluated on the domain [-L,L]

%c_G   = Alpha .* gamma( (1+Alpha)/2.0 )./...
%        ( 2*pi^(0.5 + Alpha) * gamma( (2-Alpha)/2.0 ) );

 c_G   = 2^(Alpha-1)*Alpha .* gamma( (1+Alpha)/2.0 )./...
          ( pi^0.5 * gamma( (2-Alpha)/2.0 ) );
    
G  = zeros(N,1);       % G at x=0 will be evaluated as 0

xf = linspace(-L, L, N+1);
G(1:(N-1)/2)   = c_G/Alpha * ( ( -xf(2:(N+1)/2) ).^(-Alpha) ...
                             - ( -xf(1:(N-1)/2) ).^(-Alpha));
G((N+3)/2:end) = -c_G/Alpha * ( xf((N+5)/2:end).^(-Alpha) ...
                              - xf((N+3)/2:end-1).^(-Alpha));    

if(isrow(G))
   G = G'; % Changing to column vector
end         
             

Gb = c_G/(Alpha*L^Alpha);
                         
end