% Plots solution at final time

function error = MLMC_error(StatL,config)

assert(exist(config.exp_ref_file, 'file')~=0,...
       'Reference statistics file %s does not exist||',config.exp_ref_file);
load(config.exp_ref_file);

ref_mean = REF_EXP{1};
ref_x    = REF_EXP{2};
mean     = StatL{1};
x        = StatL{3};   
ref_mean_pr = interp1(ref_x,ref_mean,x);
dx = x(2) - x(1); 
% plot(x,mean,x,ref_mean_pr)
% pause(1)
error = 100*sqrt(sum((mean-ref_mean_pr).^2)*dx)/sqrt(sum((ref_mean_pr).^2)*dx);

end