function A = eval_Au(Af,Un,N,bc,omega)

% Evaluated on extended domain [-2L+dx/2,2L-dx/2]

% N = 2M + 1
M = (N-1)/2;

U = zeros(4*M+1,1);
U(1:M) = bc*ones(M,1)*Un(1);
U(M+1:3*M+1)=Un;
U(3*M+2:end) = bc*ones(M,1)*Un(end);

A = Af(U,omega);

end