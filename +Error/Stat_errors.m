% Plots expectation RMSE errors

function Stat_errors(Errors)

figure(1)
clf
plot(x,mean,'k','LineWidth',2);
plot(x,mean+std,'r-.','LineWidth',2);
plot(x,mean-std,'b--','LineWidth',2);
legend([h1,h2,h3],{'Mean','Mean + Std','Mean - Std'},'Location','Best')
set(hf,'EdgeColor','None');
hold off


set(gca,'fontsize',20)
xlabel('x')
title(['Statistics at time t = ',num2str(Tf)])
box
outfile = sprintf('Output/MLMC_Variance.pdf');
axis tight
ylim([-1.3,1.3])
print('-dpdf', outfile)

end