% Plots solution at final time

function MCstats(StatL,config)

    Nlevels = length(StatL);
    legend_text = cell(Nlevels,1);
    Tf = config.Tf;

    figure(1)
    clf
    hold all

    figure(2)
    clf
    hold all

    for i=1:Nlevels
        figure(1)
        plot(StatL{i}{3},StatL{i}{1},'LineWidth',2)

        figure(2)
        plot(StatL{i}{3},StatL{i}{2},'LineWidth',2)

        legend_text{i} = sprintf('N = %d',length(StatL{i}{3}));
    end

    figure(1)
    set(gca,'fontsize',20)
    xlabel('x')
    ylabel('Sample Mean')
    legend(legend_text,'Location','Best')
    title(['Statistics at time t = ',num2str(Tf)])
    box
    print -dpdf 'Output/MC_Mean.pdf'
    
    
    

    figure(2)
    set(gca,'fontsize',20)
    xlabel('x')
    ylabel('Sample Variance')
    title(['Statistics at time t = ',num2str(Tf)])
    legend(legend_text,'Location','Best')
    box
    print -dpdf 'Output/MC_Variance.pdf'
    
    


end