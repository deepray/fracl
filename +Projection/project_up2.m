function val0 = project_up2(val0,l,N,Nref)
   
   ind_inc   = 3^(Nref-l+1);
   shift     = 3^(Nref-l);
   ind_s     = (ind_inc + 1)/2;
   
%    figure(500)
%    plot(val0,'--o')
%    hold all

   val0(ind_s - shift) = val0(ind_s);
   
   for i = ind_s:ind_inc:N-ind_inc
       ind1 = i+shift;
       ind2 = i+2*shift;
       val0(ind1) = (val0(i)*2 + val0(i+ind_inc))/3.0;
       val0(ind2) = (val0(i) + val0(i+ind_inc)*2)/3.0;
   end

   val0(i+ind_inc+shift) = val0(i+ind_inc);
   
%    plot(val0,'--*');
%    hold off
%    
%    pause(3)

end