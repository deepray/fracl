function [StatL] = MC_eval(config)
    
	config.processInput();
  
    Nref = config.Levels;
    Nc   = config.Nc0*ones(1,Nref).*3.^(0:Nref-1); % Various MC levels
    
    Samples = config.Samples;
    
    assert(Samples>1,'Must choose at least two samples for MC solver');
    
    f_rvar = load('Random_data/flux_rvar.dat');
    ic_rvar = load('Random_data/ic_rvar.dat');
    A_rvar = load('Random_data/A_rvar.dat');
    
    sfn  = config.n_flux_rvar;
    sicn = config.n_ic_rvar;
    sAn  = config.n_A_rvar;
    
    StatL = cell(Nref,1);
   
    for l = 1:Nref
        sf_rvar = [];
        sic_rvar = [];
        sA_rvar = [];
        
        Exp = zeros(Nc(l),1);
        S2  = zeros(Nc(l),1);
        Delta = zeros(Nc(l),1);
        
        % Using same set of random variables for each mesh level
        sfn_ind_s  = 1;
        sicn_ind_s = 1;
        sAn_ind_s  = 1;
        
        fprintf('Running %d samples on mesh with N = %d : ',Samples,Nc(l));
        for s = 1:Samples
            
            prog_level(ceil(s/Samples*100));
            if(sfn > 0)
                sf_rvar = f_rvar(sfn_ind_s:sfn_ind_s + sfn-1);
                sfn_ind_s = sfn_ind_s + sfn;
            end
            if(sicn > 0)
                sic_rvar = ic_rvar(sicn_ind_s:sicn_ind_s + sicn-1);
                sicn_ind_s = sicn_ind_s + sicn;
            end
            if(sAn > 0)
                sA_rvar = A_rvar(sAn_ind_s:sAn_ind_s + sAn-1);
                sAn_ind_s = sAn_ind_s + sAn;
            end
            
            soln = config.scheme(config,Nc(l),sf_rvar,sic_rvar,sA_rvar,0);
            
            Delta = soln{1} - Exp;
            Exp   = Exp + Delta/s;
            S2    = S2 + Delta.*(soln{1} - Exp);
%             Exp = Exp + soln{1};
%             S2  = S2  + soln{1}.^2;
            
            if(s<Samples)
                clear_progress();
            end
        end
        fprintf('\n')
        %Exp = Exp/Samples;
        %S2  = S2/Samples;
        %StatL{l} = {Exp,S2 - Exp.^2,soln{2}};
        
        % StatL{l} corresponds to the statsitics at mesh level l
        StatL{l} = {Exp,S2/(Samples-1),soln{2}};
    end
end