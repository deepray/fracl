function REF_EXP = ref_exp(config,Nc,NRVAR)

    config.processInput();
    
    U     = zeros(Nc,1);
    RVAR  = linspace(0,1,NRVAR);
   
    
    sfn  = config.n_flux_rvar;
    sicn = config.n_ic_rvar;
    sAn  = config.n_A_rvar;
    
    sf_rvar = [];
    sic_rvar = [];
    sA_rvar = [];
    
    rvec = ones(1,sfn+sicn+sAn);
    
    STOP = 0;
    
    samples = NRVAR^(sfn+sicn+sAn);
    si = 1;
    
    fprintf('\nGenerating reference expecation\n')
    
    while(~STOP)
        
        
        prog_level(ceil(si/samples*100));
        if(sfn > 0)
            sf_rvar  = RVAR(rvec(1:sfn));
        end
        if(sicn > 0)
            sic_rvar = RVAR(rvec(sfn+1:sfn+sicn));
        end
        if(sAn > 0)
            sA_rvar  = RVAR(rvec(sfn+sicn+1:sfn+sicn+sAn));
        end

        w = 1/2^(sum(rvec==1) + sum(rvec==NRVAR));

        soln = config.scheme(config,Nc,sf_rvar,sic_rvar,sA_rvar,0);
        plot(soln{2},soln{1},'--')
        hold all
        pause(0.0001)
        U    = U + w*soln{1};
        [rvec,STOP] = Misc.incrementer(rvec,NRVAR);
        
        if(si<samples)
            clear_progress();
        end
        
        si = si+1;
    end
    
    
    REF_EXP{1} = U*(RVAR(2) - RVAR(1))^(sfn+sicn+sAn);
    REF_EXP{2} = soln{2};
    
    plot(REF_EXP{2},REF_EXP{1},'k-','LineWidth',3)
    title('Reference expectation')
    xlabel('x')
    ylabel('Exp')
    
    save(config.exp_ref_file,'REF_EXP')
    
end