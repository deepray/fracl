function val0 = project_up(val0,l,N,Nref)
   
   ind_inc   = 3^(Nref-l+1);
   shift     = 3^(Nref-l);
   ind_s     = (ind_inc + 1)/2;

   for i = ind_s:ind_inc:N
       ind = [i-shift,i+shift];
       val0(ind) = val0(i)*ones(2,1);
   end


end