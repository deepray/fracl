function burgers_create_reference()
	%% Set configurations
	config = Config;

	config.model = Model.Burgers;
	config.Tf    = 0.5;
	config.CFL   = 0.8;
	config.L     = 1;	
    config.Nc0   = 51;
    config.Levels= 3;
    
	config.n_ic_rvar = 0;
    config.ic    = @(x,omega) 1*(x<0.1) - 1*(x>=0.1);
 
    config.bc    = 'Transmissive';
    config.n_A_rvar = 1;
    config.A     = @(U,omega) max(U,omega);
    config.Alpha = 0.5; 
    config.advection = true;
    config.scheme = @Scheme.explicit_scheme;
    config.exp_ref_file = 'Reference/ref4.mat';
    
    %% Find reference Expectation
	EXP = Solver.par_ref_exp(config,4001,4);
end