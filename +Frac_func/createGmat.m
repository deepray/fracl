function Gmat = createGmat(G,Gb)

N    = length(G);
M    = (N-1)/2;  % N = 2M+1

Gmat = zeros(N,N);

P = sum(G) + Gb;

% First column 
Gmat(:,1) = -Gb*ones(N,1);
for i=1:M+1
    Gmat(i,1) = Gmat(i,1) - sum(G(1:M+2-i));
end

% Last column 
Gmat(:,N) = -Gb*ones(N,1);
for i=M+1:N
    Gmat(i,N) = Gmat(i,N) - sum(G(end+M+1-i:end));
end


% Columns 2 to N-1
for i=1:M
    Gmat(i,2:M+i) = -G(M+3-i:end);
end
    
Gmat(M+1,2:end-1) = -G(2:end-1);

for i=1:M
    Gmat(M+1+i,i+1:end-1) = -G(1:end-1-i);
end

% Adding P*I
Gmat = Gmat + P*eye(N,N);

end