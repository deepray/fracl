% Plots solution at final time

function userplotscript(soln,config)

Tf = config.Tf;
Nref = config.Levels;
legend_text = cell(Nref+1,1);

figure(1)
clf
hold all


plot(soln{1}{2},config.ic(soln{1}{2}),'--','LineWidth',2)    
legend_text{1} = 'Initial condition';
for l=1:Nref
   plot(soln{l}{2},soln{l}{1},'-','LineWidth',2)    
   legend_text{l+1} = sprintf('Num. soln., N = %d',length(soln{l}{2}));
end
set(gca,'fontsize',20)

xlabel('x')
ylabel('u')
legend(legend_text,'Location','northeast')
box
print -dpdf -bestfit 'Output/Solution.pdf'

end