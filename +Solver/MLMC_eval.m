% StatL stores the final statistics ONLY
% Expectation is given by:
%    $E_L = sum_{l=1}^Levels \Delta E_l   + E_0$
%    \Delta E_l = Exp_{M_l} [ u_l - u_{l-1} ]
%    E_0 = Exp_{M_0} [u_0]
%
% Variance is given by:
%    $V_L = sum_{l=1}^Levels \Delta V_l   + V_0$
%    \Delta V_l = Exp_{M_l} [ (u_l - u_{l-1} - \Delta E_l )^2 ]
%    V_0 = Exp_{M_0} [ (u_0 - E_0)^2 ] 
%
% To evaluate these quantities in a stable manner, we use the following
% stable algorithm
% 
% Exp = 0
% Var = 0
% Loop over levels l=1:Levels
%    Exp_l = 0
%    Var_l = 0  
%    Loop over samples s=1:S_l
%       if l == 1
%          Diff = soln_l
%       else 
%          Diff = soln_l - soln_{l-1}
%       Delta = Diff - Exp_l
%       Exp_l = Exp_l + Delta/s
%       Var_l = Var_l + Delta*(Diff - Exp_l);
%    Exp = Exp + Exp_l
%    Var = Var + Var_l/(S_l - 1)  
%    
function [StatL] = MLMC_eval(config)
	config.processInput();
    
    % Various MC levels
    Levels = config.Levels;
    Nc     = config.Nc0*ones(1,Levels+1).*3.^(0:Levels); % Various MC levels
    
    % Finest mesh
    L = config.L;
    dx  = 2*L/Nc(end);
    dx0  = 2*L/Nc(1);
    xf = linspace(-L + 0.5*dx , L - 0.5*dx , Nc(end))';
   
    
    Samples = zeros(Levels+1,1);
    if(isequal(config.scheme,@Scheme.explicit_scheme))
        Samples(1) = (1 + dx0^(0.5*config.theta)*sum(3.^( (1:1:Levels)*0.5*(config.r-config.theta) )))/dx^config.theta;
        if(Levels > 0)
            Samples(2:end) = Samples(1)*dx0^(0.5*config.theta)*3.^( -(1:1:Levels)*0.5*(config.r+config.theta) );
        end
    elseif(isequal(config.scheme,@Scheme.implicit_scheme))
        Samples(1) = (log(1/dx0)^0.5 + dx0^(0.5*config.theta)*sum(3.^( (1:1:Levels)*0.5*(config.r-config.theta) ).*((1:1:Levels)*log(3) + log(1/dx0)^0.5).^0.5  ))/dx^config.theta/log(1/dx0)^0.5;
        if(Levels > 0)
            Samples(2:end) = Samples(1)*log(1/dx0)^0.5*dx0^(0.5*config.theta)*3.^( -(1:1:Levels)*0.5*(config.r+config.theta) )./((1:1:Levels)*log(3) + log(1/dx0)).^0.5;
        end
    end
    %Samples(1:end-1)./Samples(2:end)
    Samples = ceil(Samples);
    %Samples = config.Samples*3.^(Levels:-1:0);
    
    if(config.use_stored_rand_data)
        f_rvar = load('Random_data/flux_rvar.dat');
        ic_rvar = load('Random_data/ic_rvar.dat');
        A_rvar = load('Random_data/A_rvar.dat');
    end
    
    sfn  = config.n_flux_rvar;
    sicn = config.n_ic_rvar;
    sAn  = config.n_A_rvar;
    
    sfn_ind_s  = 1;
    sicn_ind_s = 1;
    sAn_ind_s  = 1;
    
    Exp    = zeros(Nc(end),1);
    Var    = zeros(Nc(end),1);
    
    
    for l = 0:Levels
        fprintf('Running %d samples on level %d with mesh having N = %d cells: ',Samples(l+1),l,Nc(l+1));
        sf_rvar = [];
        sic_rvar = [];
        sA_rvar = [];
        
        Exptemp = zeros(Nc(end),1);
        Vartemp = zeros(Nc(end),1);
        
%         ind_inc  = 3^(Nref-l);
%         ind_s    = (ind_inc + 1)/2;
%         ind1     = ind_s:ind_inc:Nc(end);

        for s = 1:Samples(l+1)
            
            prog_level(floor(s/Samples(l+1)*100));
            % Setting random paramters
            if(sfn > 0)
                if(config.use_stored_rand_data)
                    sf_rvar = f_rvar(sfn_ind_s:sfn_ind_s + sfn-1);
                    sfn_ind_s = sfn_ind_s + sfn;
                else
                    sf_rvar = rand(sfn,1);
                end
            end
            if(sicn > 0)
                if(config.use_stored_rand_data)
                    sic_rvar = ic_rvar(sicn_ind_s:sicn_ind_s + sicn-1);
                    sicn_ind_s = sicn_ind_s + sicn;
                else
                    sic_rvar = rand(sicn,1);
                end
            end
            if(sAn > 0)
                if(config.use_stored_rand_data)
                    sA_rvar = A_rvar(sAn_ind_s:sAn_ind_s + sAn-1);
                    sAn_ind_s = sAn_ind_s + sAn;
                else
                    sA_rvar = rand(sAn,1);
                end
            end
            
            % Fine mesh evaluation
            soln1 = config.scheme(config,Nc(l+1),sf_rvar,sic_rvar,sA_rvar,0);
            s1    = Projection.project_up3(soln1{2},soln1{1},l,xf,Levels);
            Diff  = s1;
            
%             figure(100)
%             plot(xf,s1,'-o')
%             ylim([0,1])
%             title([num2str(sic_rvar),' ',num2str(sA_rvar),' ',num2str(sf_rvar)])
%             pause(0.1)

            

            % Coarse mesh evaluation if l>1
            if(l>0)
                soln0    = config.scheme(config,Nc(l),sf_rvar,sic_rvar,sA_rvar,0);  
                s0       = Projection.project_up3(soln0{2},soln0{1},l-1,xf,Levels);
                Diff     = Diff - s0;
%                 figure(100)
%                 plot(xf,s0,'-o')
%                 title([num2str(sic_rvar),' ',num2str(sA_rvar),' ',num2str(sf_rvar)])
%                 ylim([0,1])
%                 pause(0.1)
            end
            
            Delta   = Diff - Exptemp;
            Exptemp = Exptemp + Delta/s;
            Vartemp = Vartemp + Delta.*(Diff - Exptemp);   
                             
            if(s<Samples(l+1))
              clear_progress();
            end
        end
        clear_progress();
        prog_level(100);
        fprintf('\n')
        
        Exp = Exp + Exptemp;    
        Var = Var + Vartemp/(Samples(l+1) - 1);
       
        clear s0 s1;
    end
    StatL = {Exp,Var,xf};
    
end