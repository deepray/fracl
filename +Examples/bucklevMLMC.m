function bucklevMLMC()
	%% Set configurations
	config = Config;

	config.model = Model.BuckLev;
	config.Tf    = 0.2;
	config.CFL   = 0.4;
	config.L     = 1;	
    config.Levels = 4;
    config.Nc0   = 41;
    
	config.n_ic_rvar = 1;
    config.ic    = @(x,omega) 0.85*(x>=-0.5+0.1*omega).*(x<0) + 0.1;
 
    config.bc    = 'Transmissive';
    config.n_A_rvar = 1;
    config.A     = @(U,omega) max(U,0.4*omega);
    config.dA    = @(U,omega) 0*(U<0.4*omega) + (U>=0.4*omega);
    config.n_flux_rvar = 1;
    config.Alpha = 1.1; 
    config.advection = true;
    config.scheme = @Scheme.implicit_scheme;
    config.exp_ref_file = 'Reference/BL_alpha_1p1.mat';

	%% Run solver
	[StatL] = Solver.MLMC_eval(config);
    
    
	%% Display data
	Plot.MLMCstats(StatL,config);


end