function soln = runSolver(config)

    config.processInput();
    Nref = config.Levels;
    Nc   = config.Nc0*ones(1,Nref).*3.^(0:Nref-1);

    sf_rvar = [];
    sic_rvar = [];
    sA_rvar = [];

    soln = cell(Nref,1);

    for l = 1:Nref
        fprintf('Running simulation on mesh with N=%d cells: ',Nc(l))
        soln{l} = config.scheme(config,Nc(l),sf_rvar,sic_rvar,sA_rvar,1);
        fprintf('\n')
        
    end

end