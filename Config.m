classdef Config < handle
    
    properties
        model                   % model to be used. 
        L                     	% limit for domain [-L,L].
        Nc0                     % Number of cells for coarsest mesh.This 
                                % number must be odd.
        Levels = 1              % Number of levels for MLMC. Default value
                                % is 1 which corresponds to MLMC with levels
                                % 0 and 1. This can be also used
                                % to define levels of refinements for
                                % a deterministic solve.
        Samples = 2             % Number of samples used for MC simulation.       
        A                       % function A(u) appearing in dissipation 
                                % term: must be a function
                                % taking a input vector U and perturbation
                                % omega, and return the 
                                % value of A at those points.
        dA                      % derivative of A(U) with respect to U,
                                % needed for implicit solver: must be a 
                                % function taking a input vector U and 
                                % perturbation omega, and return the 
                                % value of dA at those points.                        
        Alpha                   % fractional coefficient.                        
        Tf                      % final time.
        CFL = 0.5               % CFL number.
        dt  = 0.0               % Fixed time step for simulation.
                                % If non-zero dt is chosen, then CFL is
                                % ignored.
        ic                      % Initial condition: must be a function 
                                % taking an input vector x and perturbation 
                                % omega and returning the 
                                % initial data at those points.
        bc = 'Dirichlet'        % Boundary condition: must be either 
                                % 'Dirichlet' or 'Transmissive'
        advection  = true       % if this value is false, then
                                % advection term is ignored
                                
        n_flux_rvar = 0         % number of iid random variables needed to
                                % perturb the flux
        n_A_rvar = 0            % number of iid random variables needed to
                                % perturb the diffusion function A.   
        n_ic_rvar = 0           % number of iid random variables needed to
                                % perturb the initial condition. 
        
        scheme = @Scheme.explicit_scheme  % explicit_scheme or implicit_scheme  
        
        exp_ref_file = 'Reference/ref.mat'  % Path of matlab file where the 
                                            % reference expectation is stored
                                            
        use_stored_rand_data = false        % If true, then random variables
                                            % chosen from stroed files
        r                                   % MLMC parameter. Depends on 
                                            % scheme and Alpha
        theta                               % converge rate of deterministic
                                            % scheme, depending on the
                                            % Alpha and scheme
         
    end
    
    methods
        % Verify user input is correct
		function processInput(obj)
			% Check that neccessary variables have been set
			obj.checkVar('model');
			obj.checkVar('L');
            assert(isreal(obj.L) && obj.L > 0, ...
				'The Configuration parameter ''L'' must be a positive number');
            obj.checkVar('Nc0');
            assert(floor(obj.Nc0)==obj.Nc0 && obj.Nc0 > 0 && mod(obj.Nc0,2) == 1, ...
				'The Configuration parameter ''Nc0'' must be a odd positive integer');
            obj.checkVar('Levels');
            assert(floor(obj.Levels)==obj.Levels && obj.Levels > 0, ...
				'The Configuration parameter ''Nref'' must be a positive integer');
            obj.checkVar('Samples');
            assert(floor(obj.Samples)==obj.Samples && obj.Samples > 0, ...
				'The Configuration parameter ''Samples'' must be an integer > 1');
			obj.checkVar('A');
            obj.checkVar('Alpha');
			assert(isreal(obj.Alpha) && obj.Alpha > 0 && obj.Alpha < 2, ...
				'The Configuration parameter ''Alpha'' must be a number strictly in (0,2)');
            obj.checkVar('Tf');
			assert(isreal(obj.Tf) && obj.Tf > 0, ...
				'The Configuration parameter ''Tf'' must be a positive number');
            obj.checkVar('CFL');
            assert(isreal(obj.CFL) && obj.CFL > 0, ...
				'The Configuration parameter ''CFL'' must be a positive number');
            obj.checkVar('dt');
            assert(isreal(obj.dt) && obj.dt >= 0, ...
				'The Configuration parameter ''dt'' must be a non-negative number');
			obj.checkVar('ic');
            obj.checkVar('advection');
            assert(islogical(obj.advection), ...
				'The Configuration parameter ''advection'' must be true or false');
            obj.checkVar('bc');
            assert(strcmp(obj.bc,'Dirichlet') || strcmp(obj.bc,'Transmissive'), ...
				'The Configuration parameter ''bc'' must be ''Dirichlet'' or ''Transmissive''');
            obj.checkVar('use_stored_rand_data');
            assert(islogical(obj.use_stored_rand_data), ...
				'The Configuration parameter ''use_stored_rand_data'' must be true or false');
            obj.checkVar('n_flux_rvar');
            assert(floor(obj.n_flux_rvar)==obj.n_flux_rvar ...
                   && obj.n_flux_rvar >= 0, ...
				'The Configuration parameter ''n_flux_rvar'' must be a non-negative integer');
            obj.checkVar('n_A_rvar');
            assert(floor(obj.n_A_rvar)==obj.n_A_rvar ...
                   && obj.n_A_rvar >= 0, ...
				'The Configuration parameter ''n_A_rvar'' must be a non-negative integer');
            obj.checkVar('n_ic_rvar');
            assert(floor(obj.n_ic_rvar)==obj.n_ic_rvar ...
                   && obj.n_ic_rvar >= 0, ...
				'The Configuration parameter ''n_ic_rvar'' must be a non-negative integer');
            obj.checkVar('exp_ref_file')
            [filepath,name,ext] = fileparts(obj.exp_ref_file);
            assert(exist(filepath,'dir') ~= 0, ...
                'The filepath for ''exp_ref_file'' does not exist');
            assert(strcmp(ext,'.mat'), ...
                'The ''exp_ref_file'' must be of type .mat');
            
            if(isequal(obj.scheme,@Scheme.explicit_scheme))
                obj.r = 3*(obj.Alpha>0)*(obj.Alpha<=1) ...
                        + (obj.Alpha+2)*(obj.Alpha>1)*(obj.Alpha<=2);
                obj.theta = (1/4.0)*(obj.Alpha>0)*(obj.Alpha<=2.0/3.0) ...
                        +  0.5*(2-obj.Alpha)/(2+obj.Alpha)*(obj.Alpha>2.0/3.0)*(obj.Alpha<=2);   
            else
                obj.r = 4*(obj.Alpha>0)*(obj.Alpha<=1) ...
                        + (obj.Alpha+3)*(obj.Alpha>1)*(obj.Alpha<=2);
                obj.theta = (1/4.0)*(obj.Alpha>0)*(obj.Alpha<=1) ...
                        +  0.25*(2-obj.Alpha)*(obj.Alpha>1)*(obj.Alpha<=2);    
            end
            
        end
    end
    
    methods (Access = private)
		% Checks that the variable member with name 'varName' is present
		% in the object, and if not, raises an error.
		function checkVar(obj, varName)			
			var = eval(['obj.' varName]);
			assert(~isempty(var), ['Configuration variable "conf.' varName '" not present']);
		end
	end
        
end