% xl -> mesh points of coarser mesh corresponing to level l
% ul -> solution on xl
% l  -> level of coarse mesh
% xf -> mesh points of fine mesh corresponing to level Nref
% We use a linear interpolation

function uf = project_up3(xl,ul,l,xf,L)
   
if(l == L)
    uf = ul;
else
    uf = zeros(length(xf),1);
    ind_inc   = 3^(L-l);
    ind_s     = (ind_inc + 1)/2;
    
    uf(1:ind_s-1) = ul(1)*ones(ind_s-1,1);
    uf(ind_s:end-ind_s+1) = interp1(xl,ul,xf(ind_s:end-ind_s+1));
    uf(end-ind_s+2:end) = ul(end)*ones(ind_s-1,1);
    
    % Sometime round of errors may lead to xf(ind_s) < xl(1) or
    % xf(end-ind_s+1 - ind_s+1) > xl(end). Thus, we hardcode these values
    uf(ind_s) = ul(1);
    uf(end-ind_s+1) = ul(end);
    
end

end