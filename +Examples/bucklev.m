function bucklev()
	%% Set configurations
	config = Config;

	config.model = Model.BuckLev;
	config.Tf    = 0.2;
	config.CFL   = 0.4;
	config.L     = 1;	
    config.Nc0   = 1001;
    
	config.ic    = @(x,omega) 0.85*(x>=-0.55).*(x<0) + 0.1;
    config.bc    = 'Transmissive';
    config.A     = @(U,omega) max(U,0.2);
    config.dA    = @(U,omega) 0*(U<0.2) + (U>=0.2);
    config.Alpha = 0.5; 
    config.advection = true;
    config.scheme = @Scheme.explicit_scheme;
    config.exp_ref_file = 'Reference/BL_alpha_0p5.mat';

	%% Run solver
    tic
	soln = Solver.runSolver(config);
    toc

	%% Display data
	%Plot.plotsoln(soln,config);	
    Plot.userplotscript(soln,config);
    
end
