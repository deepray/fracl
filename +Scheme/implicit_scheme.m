%##########################################################################
% Implicit solver for the PDE:
%    u_t + f(u)_x = - ( -Laplace )^\alpha/2 A(u) on (0,infty) \times [-L,L]
%    u(0,x) = u_0(x)
% Author: Deep Ray, TIFR-CAM
% Date  : 20 Feb 2017
%
%##########################################################################
function soln_data = implicit_scheme(config,N,sf_rvar,sic_rvar,sA_rvar,verbose)

eps1 = 1e-6;
eps2 = 1e-6;
iter_max = 100;

assert(mod(N,2) == 1,'N must be an odd number')

% Set model
model = config.model;

if(strcmp(config.bc,'Dirichlet'))
    bc = 0;
else
    bc = 1;
end
    

% Create mesh for [-L,L]
L = config.L;
dx = 2*L/N;
xc = linspace(-L + 0.5*dx , L - 0.5*dx , N)';

% Set initial condition
U0 = config.ic(xc,sic_rvar);
if(isrow(U0)) 
    U0 = U0'; 
end
Tf = config.Tf;

% Evaluation of G and the extended contribution beyond [-L,L]
% by assuming constant solution
[G,Gb] = Frac_func.eval_G(L,N,config.Alpha);

% Create Gmat needed for Jacobian
Gmat = Frac_func.createGmat(G,Gb);

% Set the function A(u) and dA(u)
Af = config.A;
dAf = config.dA;

% Set initial time to 0
t = 0;
Un = U0;

U = zeros(N+2,1);

% Main update loop
while(t<Tf)
    %dt = config.CFL/(2*(Gb + sum(G)));
    if(config.dt > 0)
        dt = config.dt;
    else
        dt = config.CFL*dx^max(1,config.Alpha);
    end
    
    if(verbose)
       prog_level(ceil(t/Tf*100));
    end
    
    % Correct if time step lead to overshoot of final time
    if(t+dt>Tf)
        dt = Tf-t;
    end
    
    beta = Un;
    % Convective flux accumulation
    if(config.advection)
        U(1) = bc*Un(1);
        U(2:end-1) = Un;
        U(end) = bc*Un(end);
        flux = model.nflux(U,sf_rvar);
        beta = beta - dt*(flux(2:end) - flux(1:end-1))/dx;
    end
    
    % Newton loop
    Us = Un;
    J = zeros(N,1);
    Js = 1;
    J0 = 1;
    iter = 0;
   
    while(Js/J0>eps1 && Js > eps2 && iter < iter_max)
        
        % Non-local diffusion terms
        A  = Frac_func.eval_Au(Af,Us,N,bc,sA_rvar);
        Ns = (N-1)/2;
        
        for i=1:N
            J(i) = - sum( G.* ( A(i:N+i-1) - A(Ns+i) ) )...
                - Gb*(A(1) + A(end) - 2*A(Ns+i)) ;
        end
        
        % Adding convection terms;
        J = Us + dt*J - beta;
        
        % Finding derivative of A
        dA = dAf(Us,sA_rvar);
        
        % Building Jacobian
        dJ = eye(N,N) + dt*Gmat.*repmat(dA',N,1);
        %dUs = dJ\(-J);
        [Lmat,Umat] = lu(dJ);
        dUs = Umat\(Lmat\(-J));
        Us = Us + dUs;
        iter = iter+1;
        Js = norm(J);
        if(iter == 1)
            J0 = Js;
        end
    end
    
    if(iter >= iter_max)
       sprintf('Warning!! Newton solver did not converge\n\n')
    end
       
    % Time update of solution
    Un  = Us;
    t   = t + dt;
    
    if(t < Tf && verbose)
        clear_progress();
    end
  
end

soln_data{1} = Un;
soln_data{2} = xc;


end


