function burgers()
	%% Set configurations
	config = Config;

	config.model = Model.Burgers;
	config.Tf    = 0.5;
	config.CFL   = 0.8;
	config.L     = 1;
    config.Levels= 4;
    config.Nc0   = 51;
    
	config.ic    = @(x,omega) 1*(x<0) -1*(x>=0);
    config.bc    = 'Transmissive';
    config.A     = @(U,omega) 0*max(U,0);
    config.dA    = @(U,omega) 0*(U<0) + 1*(U>=0);
    config.Alpha = 0.5; 
    config.advection = true;
    
    config.scheme = @Scheme.explicit_scheme;

	%% Run solver
    tic
	soln = Solver.runSolver(config);
    toc

	%% Display data
	Plot.plotsoln(soln,config);	
    %Plot.userplotscript(soln,config);
    
end