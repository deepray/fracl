function REF_EXP = par_ref_exp(config,Nc,NRVAR)

    config.processInput();
    
    U     = zeros(Nc,1);
    x     = zeros(Nc,1);
    RVAR  = linspace(0,1,NRVAR);
   
    
    sfn  = config.n_flux_rvar;
    sicn = config.n_ic_rvar;
    sAn  = config.n_A_rvar;
    
    scheme = config.scheme;

    samples = NRVAR^(sfn+sicn+sAn);
    
    fprintf('\nGenerating reference expecation\n')
    
    fprintf('Completed samples:\n')
    
    tic
    parfor s=1:samples
        
        sf_rvar = [];
        sic_rvar = [];
        sA_rvar = [];
        rvec = Misc.dec2Nbase(s,1,NRVAR,sfn+sicn+sAn);
        if(sfn > 0)
            sf_rvar  = RVAR(rvec(1:sfn));
        end
        if(sicn > 0)
            sic_rvar = RVAR(rvec(sfn+1:sfn+sicn));
        end
        if(sAn > 0)
            sA_rvar  = RVAR(rvec(sfn+sicn+1:sfn+sicn+sAn));
        end

        w = 1/2^(sum(rvec==1) + sum(rvec==NRVAR));

        soln = scheme(config,Nc,sf_rvar,sic_rvar,sA_rvar,0);
        U    = U + w*soln{1};   
        x    = x + soln{2}/samples;
        
       fprintf('... %d',s)
   
    end
    toc
    
    
    REF_EXP{1} = U*(RVAR(2) - RVAR(1))^(sfn+sicn+sAn);
    REF_EXP{2} = x;
    
    plot(REF_EXP{2},REF_EXP{1},'k-','LineWidth',3)
    title('Reference expectation')
    xlabel('x')
    ylabel('Exp')
    
    save(config.exp_ref_file,'REF_EXP')
    
end