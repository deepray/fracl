function burgersMC()
	%% Set configurations
	config = Config;

	config.model = Model.Burgers;
	config.Tf    = 0.5;
	config.CFL   = 0.1;
	config.L     = 1;	
    config.Nc0   = 101;
    config.Levels  = 1;
    config.Samples = 100;
    
	config.n_ic_rvar = 1;
    config.ic    = @(x,omega) 1*(x<0.1*omega) - 1*(x>=0.1*omega);
 
    config.bc    = 'Transmissive';
    config.n_A_rvar = 1;
    config.A     = @(U,omega) max(U,omega);
    config.Alpha = 0.5; 
    config.advection = true;
    config.scheme = @Scheme.explicit_scheme;

	%% Find Statistics
	[StatL] = Solver.MC_eval(config);


	%% Display data
	Plot.MCstats2(StatL,config);	
end