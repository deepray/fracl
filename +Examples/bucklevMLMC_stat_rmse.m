function bucklevMLMC_stat_rmse()
	%% Set configurations
	config = Config;

	config.model = Model.BuckLev;
	config.Tf    = 0.2;
	config.CFL   = 0.4;
	config.L     = 1;	
    config.Levels = 4;
    config.Nc0   = 41;
    
	config.n_ic_rvar = 1;
    config.ic    = @(x,omega) 0.85*(x>=-0.5+0.1*omega).*(x<0) + 0.1;
 
    config.bc    = 'Transmissive';
    config.n_A_rvar = 1;
    config.A     = @(U,omega) max(U,0.4*omega);
    config.dA    = @(U,omega) 0*(U<0.4*omega) + (U>=0.4*omega);
    config.n_flux_rvar = 1;
    config.Alpha = 1.1; 
    config.advection = true;
    config.scheme = @Scheme.implicit_scheme;
    config.exp_ref_file = 'Reference/BL_alpha_1p1.mat';

	%% Run solver
	[Errors] = Solver.MLMC_stat_rmse(config,30);
    Errors
    figure(1)
    plot(log(Errors(:,1)),log(Errors(:,3)))
    polyfit(log(Errors(:,1)),log(Errors(:,3)),1)
    figure(2)
    plot(log(Errors(:,2)),log(Errors(:,3)))
    polyfit(log(Errors(:,2)),log(Errors(:,3)),1)
    
	%% Display data
	%Plot.Stat_errors(Errors);


end