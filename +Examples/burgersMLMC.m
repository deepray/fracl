function burgersMLMC()
	%% Set configurations
	config = Config;

	config.model = Model.Burgers;
	config.Tf    = 0.5;
	config.CFL   = 0.5;
	config.L     = 1;	
    config.Nc0   = 51;
    config.Levels= 3;
    
	config.n_ic_rvar = 1;
    config.ic    = @(x,omega) 1*(x<0.1*omega) - 1*(x>=0.1*omega);
 
    config.bc    = 'Transmissive';
    config.n_A_rvar = 1;
    config.A     = @(U,omega) max(U,omega);
    config.Alpha = 0.5; 
    config.advection = true;
    config.scheme = @Scheme.explicit_scheme;
    config.exp_ref_file = 'Reference/ref1.mat';

	%% Run solver
	[StatL] = Solver.MLMC_eval(config);
    
    
	%% Display data
	Plot.MLMCstats(StatL,config);


end