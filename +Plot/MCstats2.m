% Plots solution at final time

function MCstats2(StatL,config)

    Nlevels = length(StatL);
    Tf = config.Tf;

    for i=1:Nlevels
        figure(1)
        clf
        x     = StatL{i}{3};
        mean  = StatL{i}{1};
        std   = sqrt(StatL{i}{2});
        
        
        hold all
        hf = fill([x',fliplr(x')],[(mean+std)',fliplr((mean-std)')],[0.8 0.8 0.9]);
        h1 = plot(x,mean,'k','LineWidth',2);
        h2 = plot(x,mean+std,'r-.','LineWidth',2);
        h3 = plot(x,mean-std,'b--','LineWidth',2);
        legend([h1,h2,h3],{'Mean','Mean + Std','Mean - Std'},'Location','Best')
        set(hf,'EdgeColor','None');
        hold off
        
      
        set(gca,'fontsize',20)
        xlabel('x')
        title(['Statistics at time t = ',num2str(Tf)])
        box
        outfile = sprintf('Output/MC_Variance_%d.pdf',i);
        axis tight
        ylim([-1.3,1.3])
        print('-dpdf', outfile)
        

    end
    


end