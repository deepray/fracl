% Runs MLMC solver to evaluate RMSE of expectation

function Errors = MLMC_stat_rmse(config,N)

Levels  = config.Levels;

Errors = zeros(Levels,3);
for i = 1:Levels
    config.Levels = i;
    for j = 1:N
        tic 
        [StatL] = Solver.MLMC_eval(config);
        Errors(i,1) = Errors(i,1) + toc;
        x = StatL{3};
        Errors(i,2) = x(2) - x(1);
        Errors(i,3) = Errors(i,3) + (Error.MLMC_error(StatL,config)).^2;
        clear StatL
    end
    Errors(i,1) = Errors(i,1)/N;
    Errors(i,3) = sqrt(Errors(i,3)/N);
end
