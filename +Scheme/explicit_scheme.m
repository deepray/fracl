%##########################################################################
% Explicit solver for the PDE:
%    u_t + f(u)_x = - ( -Laplace )^\alpha/2 A(u) on (0,infty) \times [-L,L]
%    u(0,x) = u_0(x)
% Author: Deep Ray, TIFR-CAM
% Date  : 20 Feb 2017
%
%##########################################################################
function soln_data = explicit_scheme(config,N,sf_rvar,sic_rvar,sA_rvar,verbose)

assert(mod(N,2) == 1,'N must be an odd number')

% Set model
model = config.model;

if(strcmp(config.bc,'Dirichlet'))
    bc = 0;
else
    bc = 1;
end
    

% Create mesh for [-L,L]
L = config.L;
dx = 2*L/N;
xc = linspace(-L + 0.5*dx , L - 0.5*dx , N)';

% Set initial condition
U0 = config.ic(xc,sic_rvar);
if(isrow(U0)) 
    U0 = U0'; 
end
Tf = config.Tf;

% Evaluation of G and the extended contribution beyond [-L,L]
% by assuming constant solution
[G,Gb] = Frac_func.eval_G(L,N,config.Alpha);

% Set the function A(u)
Af = config.A;

% Set initial time to 0
t = 0;
Un = U0;

U = zeros(N+2,1);

% Main update loop
while(t<Tf)
    if(config.dt > 0)
        dt = config.dt;
    else
        dt = config.CFL*dx^max(1,config.Alpha);
    end
    if(verbose)
       prog_level(ceil(t/Tf*100));
    end
    
    % Correct if time step lead to overshoot of final time
    if(t+dt>Tf)
        dt = Tf-t;
    end
    
    res  = zeros(N,1);
    
    % Convective flux accumulation
    if(config.advection)
        U(1) = bc*Un(1);
        U(2:end-1) = Un;
        U(end) = bc*Un(end);
        flux = model.nflux(U,sf_rvar);
        res = -(flux(2:end) - flux(1:end-1))/dx;
    end
    
    % Non-local diffusion terms
    A  = Frac_func.eval_Au(Af,Un,N,bc,sA_rvar);
    Ns = (N-1)/2;
 
    for i=1:N
        res(i) = res(i) + sum( G.* ( A(i:N+i-1) - A(Ns+i) ) )...
                        + Gb*(A(1) + A(end) - 2*A(Ns+i)) ;
    end
    

    % Time update of solution
    Un  = Un + dt*res;
    t   = t + dt;
    
    if(t < Tf && verbose)
        clear_progress();
    end
    
    
    %sprintf('dt=%f',dt)
  
end

soln_data{1} = Un;
soln_data{2} = xc;


end


