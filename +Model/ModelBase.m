classdef ModelBase < handle
	%MODEL   Abstract base class for all model classes.
	
	properties (Abstract, SetAccess = private)
		name	% Name of model
    end
		
	methods (Abstract)
		% Flux function
		ret = f(o, U, omega);
    end
    
    methods (Abstract)
		% Flux Jacobian function
		ret = df(o, U, omega);
    end
	
	
end