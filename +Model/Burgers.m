classdef Burgers < Model.ModelBase
	%BURGERS   Burgers' equation
	
	properties (SetAccess = private)
		name = 'Burgers'
	end
	
	methods
		function ret = f(obj, U, omega)
			ret = U.*U / 2;
		end
    end
    
    methods
		function ret = df(obj, U, omega)
			ret = U;
		end
    end
    
    methods
		function ret = nflux(obj, U, omega) 
            alam = abs(obj.df(U,omega));
            ret = 0.5*( obj.f(U(2:end),omega) + obj.f(U(1:end-1),omega) ...
                - (U(2:end)- U(1:end-1)).*max(alam(1:end-1),alam(2:end)) );
		end
    end
    
end