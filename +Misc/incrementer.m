% Incrementer

function [y,STOP] = incrementer(x,N)

y = x; % DEFAULT
val = x(end);
if val < N
    x(end) = x(end) + 1;
    y = x;
    STOP = 0;
else
    if(length(x) > 1 )
        [z,STOP] = Misc.incrementer(x(1:end-1),N);
        if (~STOP)
            y = [z,1];
            STOP = 0;
        end
    else
        y = x;
        STOP = 1;
    end
end

return;