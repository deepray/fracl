function bucklevMC()
	%% Set configurations
	config = Config;

	config.model = Model.BuckLev;
	config.Tf    = 0.2;
	config.CFL   = 0.5;
	config.L     = 1;	
    config.Nc0   = 101;
    config.Samples = 101;
    
	config.n_ic_rvar = 1;
    config.ic    = @(x,omega) 0.85*(x>=-0.5).*(x<0) + 0.1*omega;
 
    config.bc    = 'Transmissive';
    config.n_A_rvar = 1;
    config.A     = @(U,omega) max(U,omega);
    config.dA    = @(U,omega) 0*(U<omega) + (U>=omega);
    config.n_flux_rvar = 1;
    config.Alpha = 0.5; 
    config.advection = true;
    config.scheme = @Scheme.explicit_scheme;

	%% Find Statistics
	[StatL] = Solver.MC_eval(config);


	%% Display data
	Plot.MCstats2(StatL,config);	
end